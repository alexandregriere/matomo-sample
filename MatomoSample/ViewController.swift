//
//  ViewController.swift
//  MatomoSample
//
//  Created by Alexandre GRIERE on 09/01/2020.
//  Copyright © 2020 Alexandre GRIERE. All rights reserved.
//

import UIKit
import MatomoTracker //Don't forget to import the module

class ViewController: UIViewController {
    
    //variable in case we need to use it multiple time in the same view controller
    var matomoTracker2: MatomoTracker? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // We're getting the matomo instance from the appdelegate.
        // this can be every time we need it...
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let matomoTracker = appDelegate.matomoTracker
        
        //... or we store it in the current ViewController
        let appDelegate2 = UIApplication.shared.delegate as! AppDelegate
        self.matomoTracker2 = appDelegate2.matomoTracker
        
        
        //We can now use the tracker
        matomoTracker.track(view: ["path","to","your","page"])
        
    }


}

